﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Falcon.Data.Local;

namespace Falcon
{
    public class Main
    {
        public DataBase database = new DataBase();
        public void Start()
        {
            database.Update();
        }
    }
    public static class VersionControl
    {
        private const string version = "0.0.1";
        private const string autor = "Mohammad Ali Fathi";
        public static string GetVersion() { return version; }
        public static string GetAutor() { return autor; }
        public static string GetInfo() { return $"Autor : {autor} | Version : {version}"; }
    }
}