﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falcon.Data.Local
{
    public class DataBaseTable
    {
        #region Properties
        private static int _table_count;
        private DataTable _data_table;
        #endregion

        #region Constructors
        public DataBaseTable() : this(string.Format("table_{0}", _table_count++)) { }
        public DataBaseTable(string Name)
        {
            _data_table = new DataTable(Name);
        }
        public DataBaseTable(string Name, params ValueTuple<string, Type>[] Columns)
        {
            using(DataTable table = new DataTable(Name))
            {
                for (int i = 0; i < Columns.Length; i++)
                {
                    table.Columns.Add(Columns[i].Item1, Columns[i].Item2);
                }
                _data_table = table;
            }
        }
        #endregion

        #region Methods

        #endregion
    }
}